# libev timer examples

Simple implementations of the different ways to implement timers as described in the [ev_timer section](http://pod.tst.eu/http://cvs.schmorp.de/libev/ev.pod#code_ev_timer_code_relative_and_opti) of libev [documentation](http://pod.tst.eu/http://cvs.schmorp.de/libev/ev.pod).

This projects uses the implementations of the echo server and client using libev provided by [Tech Journey](http://codefundas.blogspot.com/2010/09/create-tcp-echo-server-using-libev.html). 

## Description
### timer\_1,2,3
Start a timer, every input from the standard input (stdin) resets the timer to its initial value.
When the timer expires (after 10 seconds), it simply prints "timeout" and restarts.

### timer\_4
Starts an echo server, and you can connect to it with one or several *echo\_client* programs.
It sets a timer for every client connection. Any input from a client resets the timer for that client to the initial value.
For the implementation details, one can refer to the [ev_timer section](http://pod.tst.eu/http://cvs.schmorp.de/libev/ev.pod#code_ev_timer_code_relative_and_opti).  


## Running the examples
`make`  build the example programs in the *bin* directory.
Programs timer\_1, timer\_2 and timer\_3 can be run independently. For timer\_4, also run several echo\_client which connect to it.
