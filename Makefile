CC=gcc
CFLAGS=-Wall
LDFLAGS=-lev
EXEDIR=bin
SRCDIR=src

all: $(EXEDIR)/timer_1 $(EXEDIR)/timer_2 $(EXEDIR)/timer_3 $(EXEDIR)/timer_4 $(EXEDIR)/echo-client

$(EXEDIR)/timer_1: $(SRCDIR)/timer_1.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)
	
$(EXEDIR)/timer_2: $(SRCDIR)/timer_2.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

$(EXEDIR)/timer_3: $(SRCDIR)/timer_3.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

$(EXEDIR)/timer_4: $(SRCDIR)/timer_4.c dlist.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)
	
dlist.o: $(SRCDIR)/dlist.c $(SRCDIR)/dlist.h
	$(CC) -c $^
	
$(EXEDIR)/echo-client: $(SRCDIR)/echo-client.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

clean:
	rm -f bin/* *.o 