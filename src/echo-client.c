#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>

#define PORT_NO 3033
#define BUFFER_SIZE 1024

int main()
{
int sd;
struct sockaddr_in addr;
//int addr_len = sizeof(addr);
char buffer[BUFFER_SIZE] = "";
char buffer_rcv[BUFFER_SIZE] = "";

// Create client socket
if( (sd = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
{
  perror("socket error");
  return -1;
}

bzero(&addr, sizeof(addr));

addr.sin_family = AF_INET;
addr.sin_port = htons(PORT_NO);
addr.sin_addr.s_addr =  htonl(INADDR_ANY);

// Connect to server socket
if(connect(sd, (struct sockaddr *)&addr, sizeof addr) < 0)
{
  perror("Connect error");
  return -1;
}

while (strcmp(buffer, "q") != 0)
{
  // Read input from user and send message to the server
  fgets(buffer, BUFFER_SIZE, stdin);
  int len = send(sd, buffer, strlen(buffer), 0);

  int error_code = recv(sd, buffer_rcv, BUFFER_SIZE, 0);

  // Receive message from the server
  if (error_code == -1 || error_code != len) {
            perror("recv");
            exit(1);
  }
  printf("error_code: %d\n", error_code);
  printf("message: %s\n", buffer_rcv);
}

return 0;
}
