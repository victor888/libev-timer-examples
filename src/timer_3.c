
#include <ev.h>
#include <stdio.h>
#include <unistd.h>

ev_timer timeout_watcher;
ev_timer incoming_ev_w;
ev_io stdin_watcher;

ev_tstamp timeout = 10.;
ev_tstamp last_activity; // time of last activity

static void stdin_cb(EV_P_ ev_io *w, int revents) {
    char str[50];

    if (fgets (str, 50, stdin) != NULL) {
        printf("Timer updated (%f s). You entered: %s", timeout, str);
    }
    printf("Enter a string to update timer: \n");

    last_activity = ev_now (EV_A);
}


static void callback(EV_P_ ev_timer *w, int revents) {
    // calculate when the timeout would happen
    ev_tstamp after = last_activity - ev_now (EV_A) + timeout;

    // if negative, it means we the timeout already occurred
    if (after < 0.) {
        // timeout occurred, take action
        printf("Timeout.\n");

        //       ev_break (EV_A_ EVBREAK_ONE); // exit
        last_activity = ev_now (EV_A);
        ev_timer_set (w, timeout, 0.);
        ev_timer_start (EV_A_ w);
        printf("Restart timer after timeout (%f s)\n", timeout);
    } else {
        // callback was invoked, but there was some recent
        // activity. simply restart the timer to time out
        // after "after" seconds, which is the earliest time
        // the timeout can occur.
        ev_timer_set (w, after, 0.);
        ev_timer_start (EV_A_ w);
        printf("Restart timer, timeout has not occurred (%f s)\n", timeout);
        printf("Enter a string to update timer: \n");
    }
}


int main(void) {
    // use the default event loop unless you have special needs
    struct ev_loop *loop = EV_DEFAULT;

    // initialize an io watcher, then start it
    // this one will watch for stdin to become readable
    ev_io_init(&stdin_watcher, stdin_cb, /*STDIN_FILENO*/ STDIN_FILENO, EV_READ);
    ev_io_start(loop, &stdin_watcher);

    // initialize a timer watcher, then start it
    last_activity = ev_now (EV_A);
    ev_init(&timeout_watcher, callback);
    callback(EV_A_ &timeout_watcher, 0);

    printf("Timer set to %f seconds. Enter a string to stdin to update timer: \n", timeout);

    // now wait for events to arrive
    ev_run (loop, 0);

    return 0;
}
