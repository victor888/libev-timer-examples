/*
 * A simple doubly-linked list, supporting only basic operations
 */


#ifndef _DLIST_H_
#define _DLIST_H_

#include <stdint.h>

typedef struct dlist_head {
    struct dlist_head *next;
    struct dlist_head *prev;
} dlist_head;

void dlist_init(dlist_head *head);
int32_t dlist_is_empty(dlist_head *head);
void dlist_insert(dlist_head *head, dlist_head *item);
void dlist_remove(dlist_head *at);


#endif /* _DLIST_H_ */
