#include <stdio.h>
#include <netinet/in.h>
#include <ev.h>
#include <stdlib.h>
#include <stddef.h> // for offsetof in container_of
#include <unistd.h>
#include "dlist.h"

#define PORT_NO 3033
#define BUFFER_SIZE 1024

#define container_of(ptr, type, member) __extension__ ({    \
uint8_t *__mptr = (uint8_t *)(ptr);                     \
((type *)(__mptr - offsetof(type, member))); })


typedef struct server {
    ev_tstamp       timeout;         // same value for all clients
    struct ev_loop  *loop;
    ev_io           w_accept;
    ev_timer        w_timer;         // only one timer for all clients
    dlist_head      clients;         // double-linked list
    int             sd;
    int             total_clients;   // total number of connected clients
} server;


typedef struct client {
    server      *server;             // parent server
    dlist_head  client_dlist;        // used to chain clients in a double-linked list
    ev_io       w_read;
    ev_timer    *w_timer;            // defined in server
    ev_tstamp   last_activity;
    int         client_sd;
    int         total_clients;
} client;


void accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);
void read_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);
void timer_cb (EV_P_ ev_timer *w, int revents);


int main() {
    server *svr = calloc(1, sizeof(server));
    dlist_init(&svr->clients); // initialize clients list
    svr->loop = ev_default_loop(0);
    svr->timeout = 10.;

    printf("timeout value: %f seconds\n", svr->timeout);

    struct sockaddr_in addr;

    // Create server socket
    if( (svr->sd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
       perror("socket error");
       return -1;
    }

    if (setsockopt(svr->sd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
       perror("setsockopt(SO_REUSEADDR) failed");

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT_NO);
    addr.sin_addr.s_addr = INADDR_ANY;

    // Bind socket to address
    if (bind(svr->sd, (struct sockaddr*) &addr, sizeof(addr)) != 0) {
        perror("bind error");
    }

    // Start listing on the socket
    if (listen(svr->sd, 2) < 0) {
      perror("listen error");
      return -1;
    }

    // Initialize and start a watcher to accepts client requests
    ev_io_init(&svr->w_accept, accept_cb, svr->sd, EV_READ);
    ev_io_start(svr->loop, &svr->w_accept);

    // Start infinite loop
    while (1) {
      ev_loop(svr->loop, 0);
    }

    return 0;
}


/* Accept client requests */
void accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents) {
    // Get the server struct with container_of magic macro
    server *svr = (server *) container_of(watcher, server, w_accept);

    client *clt = (client *) malloc(sizeof(client));
    clt->server = svr;
    clt->w_timer = &svr->w_timer;
    clt->last_activity = ev_now (EV_A);

    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);

    if(EV_ERROR & revents) {
        perror("got invalid event");
        return;
    }

    // Accept client request
    clt->client_sd = accept(watcher->fd, (struct sockaddr *)&client_addr, &client_len);

    if (clt->client_sd < 0) {
        perror("accept error");
        return;
    }

    svr->total_clients++;
    printf("Successfully connected with client.\n");
    printf("%d client(s) connected.\n", svr->total_clients);

    // Initialize and start watcher to read client requests
    ev_io_init(&clt->w_read, read_cb, clt->client_sd, EV_READ);
    ev_io_start(loop, &clt->w_read);

    // Add client at the end of the list of clients for managing timeouts
    dlist_insert(svr->clients.prev, &clt->client_dlist);

    if (svr->total_clients == 1) {
        printf("First client, start timer\n");
        ev_init(clt->w_timer, timer_cb);
        timer_cb(EV_A_ clt->w_timer, 0);
    }
}

/* Read client message */
void read_cb(struct ev_loop *loop, struct ev_io *watcher, int revents){
    char buffer[BUFFER_SIZE];
    ssize_t read;

    client *clt  = (client *) container_of(watcher, client, w_read);

    printf("read callback from client %d\n", clt->client_sd);

    if(EV_ERROR & revents) {
        perror("got invalid event");
        return;
    }

    // Receive message from client socket
    read = recv(watcher->fd, buffer, BUFFER_SIZE, 0);

    if (read < 0) {
        perror("read error");
        return;
    }

    if (read == 0) {
        // Stop watcher and free client if client socket is closing
        ev_io_stop(loop,watcher);
        perror("peer might closing");
        clt->server->total_clients --; // Decrement total_clients count
        printf("%d client(s) connected.\n", clt->server->total_clients);
        close(clt->client_sd);
        // if client was meant to timeout first restart timer
        if (&clt->client_dlist == clt->server->clients.next) {
            ev_timer_stop (EV_A_ clt->w_timer);
            dlist_remove(&clt->client_dlist);
            if (clt->server->total_clients >= 1) { // if there are other clients
                timer_cb (EV_A_ clt->w_timer, 0);
            }
        } else {
            dlist_remove(&clt->client_dlist);
        }
        free(clt);
        return;
    } else {
       printf("message:%s\n",buffer);
    }

    // Send message back to the client
    send(watcher->fd, buffer, read, 0);
    bzero(buffer, read);

    clt->last_activity = ev_now (EV_A);
    // Start a new timer if we process the first client and there is more than one client.
    if (&clt->client_dlist == clt->server->clients.next) {
        if (clt->server->total_clients > 1) {
            ev_timer_stop (EV_A_ clt->w_timer);
            dlist_remove(&clt->client_dlist);
            dlist_insert(clt->server->clients.prev, &clt->client_dlist);
            timer_cb (EV_A_ clt->w_timer, 0);
        }
    } else {
        dlist_remove(&clt->client_dlist);
        dlist_insert(clt->server->clients.prev, &clt->client_dlist);
    }
}


void timer_cb(EV_P_ ev_timer *w, int revents) {
    server *svr  = (server *) container_of(w, server, w_timer);
    client *clt_first  = (client *) container_of(svr->clients.next, client, client_dlist);

    printf("timer callback, first client %d\n", clt_first->client_sd);

    // calculate when the timeout would happen
    ev_tstamp after = clt_first->last_activity - ev_now (EV_A) + svr->timeout;

    // if negative, it means we the timeout already occurred
    if (after < 0.) {
        // timeout occurred, take action
        printf("timeout for client %d\n", clt_first->client_sd);
        svr->total_clients --; // Decrement total_clients count
        printf("%d client(s) connected.\n", clt_first->server->total_clients);

        close(clt_first->client_sd);
        ev_io_stop(EV_A_ &clt_first->w_read);
        dlist_remove(&clt_first->client_dlist);
        free(clt_first);

        // start timer for next client, update first client and restart timer
        if (svr->total_clients >= 1) {
            clt_first  = (client *) container_of(svr->clients.next, client, client_dlist);
            after = clt_first->last_activity - ev_now (EV_A) + svr->timeout;

            ev_timer_set (w, after, 0.);
            ev_timer_start (EV_A_ w);
        }
    } else {
        // callback was invoked, but there was some recent
        // activity. simply restart the timer to time out
        // after "after" seconds, which is the earliest time
        // the timeout can occur.
        ev_timer_set (w, after, 0.);
        ev_timer_start (EV_A_ w);
    }
}
