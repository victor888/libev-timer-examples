
#include <ev.h>
#include <stdio.h>
#include <unistd.h>

#define TIMER_AFTER 10.0 // timeout after TIMER_AFTER seconds

ev_timer timeout_watcher;
ev_io stdin_watcher;


static void stdin_cb(EV_P_ ev_io *w, int revents) {
    char str[50];

    if (fgets (str, 50, stdin) != NULL) {
        printf("Timer reset (%f s). You entered: %s", TIMER_AFTER, str);
    }
    printf("Enter a string to reset timer: \n");

    ev_timer_again (EV_A_ &timeout_watcher);
}


static void timeout_cb(EV_P_ ev_timer *w, int revents) {
    printf("Timeout.\n");
    printf("Restart timer (%f s)\n", TIMER_AFTER);
    printf("Enter a string to reset timer: \n");

    ev_timer_again(EV_A_ w);
    // this causes the innermost ev_run to stop iterating
    // ev_break (EV_A_ EVBREAK_ONE);
}


int main(void) {
    // use the default event loop unless you have special needs
    struct ev_loop *loop = EV_DEFAULT;

    // initialise an io watcher, then start it
    // this one will watch for stdin to become readable
    ev_io_init (&stdin_watcher, stdin_cb, /*STDIN_FILENO*/ STDIN_FILENO, EV_READ);
    ev_io_start (loop, &stdin_watcher);

    // initialise a timer watcher, then start it
    // simple non-repeating TIMER_AFTER second timeout
    ev_init (&timeout_watcher, timeout_cb);
    timeout_watcher.repeat = TIMER_AFTER;
    ev_timer_again (loop, &timeout_watcher);

    printf("Timer set to %f seconds. Enter a string to stdin to reset timer: \n", TIMER_AFTER);

    // now wait for events to arrive
    ev_run (loop, 0);

    return 0;
}
