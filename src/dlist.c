/*
 * Doubly linked list implementation
 */

#include "dlist.h"

void dlist_init(dlist_head *head) {
    head->next = head;
    head->prev = head;
}

int32_t dlist_is_empty(dlist_head *head) {
    return head->next == head;
}

void dlist_insert(dlist_head *head, dlist_head *item) {
    item->prev = head;
    item->next = head->next;
    item->next->prev = item;
    head->next = item;
}

void dlist_remove(dlist_head *at) {
    at->prev->next = at->next;
    at->next->prev = at->prev;
}

